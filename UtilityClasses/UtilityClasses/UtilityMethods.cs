﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses
{
    static class UtilityMethods
    {
        public static void EnsureNotNull(params object[] parameters)
        {
            foreach (object o in parameters)
            {
                if (o == null)
                    throw new ArgumentNullException();
            }
        }
    }
}
