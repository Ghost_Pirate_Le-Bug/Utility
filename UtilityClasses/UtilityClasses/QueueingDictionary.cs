﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses
{
    public class QueueingDictionary<TKey, TValue> : Dictionary<TKey, SlimLinkedListNode<TValue>>
    {
        #region Properties

        /// <summary>
        /// True to throw on invalid operations such as adding an existing key to the dictionary
        /// or False to surpress such errors
        /// </summary>
        public bool ThrowsOnArgumentException { get; set; } = true;

        /// <summary>
        /// The maximum number of entries to keep. 0 means unlimited
        /// </summary>
        public int MaximumCacheSize { get; set; } = 0;

        /// <summary>
        /// Determines whether the dictionary will overwrite the oldest entry when the selected cache size is exceeded
        /// </summary>
        public bool OverwriteOldEntries { get; set; } = true;

        /// <summary>
        /// Determines whether receiving an item via TryGetValue or Dictionary[key] 'refreshes' it by advancing it to the front of the queue, i.e. treat the item 
        /// as a new insertion after every time it is accessed
        /// </summary>
        public bool RefreshItemsOnGet { get; set; } = true;

        /// <summary>
        /// Determines whether updating an item via Dictionary[key] 'refreshes' it by advancing it to the front of the queue, i.e. treat the item 
        /// as a new insertion after every time it is accessed
        /// </summary>
        public bool RefreshItemsOnSet { get; set; } = true;

        /// <summary>
        /// Determines whether using 'ContainsKeys' item access 'refreshes' it by advancing it to the front of the queue, i.e. treat the item 
        /// as a new insertion after every time it is accessed
        /// </summary>
        public bool RefreshItemsOnContainsKey { get; set; } = true;

        public new TValue this[TKey key]
        {
            get
            {
                bool res = TryGetValue(key, out SlimLinkedListNode<TValue> node);
                if (res)
                {
                    if (RefreshItemsOnGet)
                        innerList.AdvanceNodeToFront(node);
                    return node.Value;
                }
                return default(TValue);
            }
            set
            {
                bool res = TryGetValue(key, out SlimLinkedListNode<TValue> node);
                if (res)
                {
                    if (RefreshItemsOnSet)
                        innerList.AdvanceNodeToFront(node);
                    node.Value = value;
                }
            }
        }

        #endregion

        #region Members

        protected SlimLinkedList<TValue> innerList = new SlimLinkedList<TValue>();
        protected Dictionary<SlimLinkedListNode<TValue>, TKey> ValueToKeyDict = new Dictionary<SlimLinkedListNode<TValue>, TKey>();

        #endregion

        #region Public Methods

        public void Add(TKey key, TValue value)
        {
            UtilityMethods.EnsureNotNull(key, value);
            if (!ContainsKey(key))
                Add(key, new SlimLinkedListNode<TValue>(value));
            else
            {
                if (ThrowsOnArgumentException)
                    ThrowArgsExceptionIfNeeded();
            }

        }

        public new void Add(TKey key, SlimLinkedListNode<TValue> value)
        {
            UtilityMethods.EnsureNotNull(key, value);
            if (Count >= MaximumCacheSize && MaximumCacheSize != 0)
            {
                if (!OverwriteOldEntries)
                    throw new InvalidOperationException("Exceeded allocated items buffer\r\n" +
                        "Consider setting 'OverwriteOldEntries' to true");
                OverwriteOldestNode(key, value);
            }
            else
                RegisterEntry(key, value);
        }

        public new bool ContainsKey(TKey key)
        {
            UtilityMethods.EnsureNotNull(key);
            bool res = base.TryGetValue(key, out SlimLinkedListNode<TValue> value);
            if (res && RefreshItemsOnContainsKey)
                innerList.AdvanceNodeToFront(value);
            return res;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            UtilityMethods.EnsureNotNull(key);
            bool res = base.TryGetValue(key, out SlimLinkedListNode<TValue> node);
            value = node.Value;
            if (res && RefreshItemsOnGet)
                innerList.AdvanceNodeToFront(node); ;
            return res;
        }

        public new bool TryGetValue(TKey key, out SlimLinkedListNode<TValue> value)
        {
            UtilityMethods.EnsureNotNull(key);
            bool res = base.TryGetValue(key, out value);
            if (res && RefreshItemsOnGet)
                innerList.AdvanceNodeToFront(value);
            return res;
        }
        
        public new bool Remove(TKey key)
        {
            UtilityMethods.EnsureNotNull(key);
            if (!base.TryGetValue(key, out SlimLinkedListNode<TValue> node))
                return false;
            ValueToKeyDict.Remove(innerList.Head);
            innerList.Remove(node);
            base.Remove(key);
            return true;
        }

        public Queue<TValue> ToQueue()
        {
            return new Queue<TValue>(from node in innerList.GetAllNodes() select node.Value);
        }

        #endregion

        #region Protected

        protected void OverwriteOldestNode(TKey key, SlimLinkedListNode<TValue> value)
        {
            if (!Remove(ValueToKeyDict[innerList.Head]))
                throw new InvalidOperationException("Could not overwrite oldest node");
            RegisterEntry(key, value);
        }
        protected void RegisterEntry(TKey key, SlimLinkedListNode<TValue> value)
        {
            innerList.Add(value);
            base.Add(key, value);
            ValueToKeyDict.Add(value, key);
        }
        protected void ThrowArgsExceptionIfNeeded()
        {
            if (ThrowsOnArgumentException)
                throw new ArgumentException();
        }
        protected void ThrowArgNullExceptionIfNeeded()
        {
            if (ThrowsOnArgumentException)
                throw new ArgumentNullException();
        }

        #endregion
    }
}