﻿using System;

namespace UtilityClasses
{
    public class SlimLinkedList<T>
    {
        public SlimLinkedListNode<T> Head { get; set; }
        public SlimLinkedListNode<T> Tail { get; set; }
        public int Count { get; set; }

        public void Add(SlimLinkedListNode<T> node)
        {
            if (Head == null)
                Head = node;
            else
            {
                Tail.Next = node;
                node.Previous = Tail;
            }
            Tail = node;
            Count++;
        }

        public void Remove(SlimLinkedListNode<T> node)
        {
            if (Count == 1)
            {
                if (node != Head && node != Tail)
                    throw new Exception("Unexpected node received");
                Head = Tail = null;
            }
            else
            {
                if (node == Head)
                {
                    node.Next.Previous = null;
                    Head = node.Next;
                }
                else
                {
                    if (node == Tail)
                    {
                        node.Previous.Next = null;
                        Tail = node.Previous;
                    }
                    else
                    {
                        node.Previous.Next = node.Next;
                        node.Next.Previous = node.Previous;
                    }
                }                
            }
            // Node is not head or tail and list contains more than one item
            
            node.Dispose();
            Count--;
        }

        public SlimLinkedListNode<T>[] GetAllNodes()
        {
            if (Count == 0)
                return new SlimLinkedListNode<T>[0];
            SlimLinkedListNode<T>[] allNodes = new SlimLinkedListNode<T>[Count];
            SlimLinkedListNode<T> currNode = Head;
            allNodes[0] = Head;
            for (int i = 1; i < allNodes.Length; i++)
            {
                currNode = currNode.Next;
                allNodes[i] = currNode;
            }
            return allNodes;
        }

        public void AdvanceNodeToFront(SlimLinkedListNode<T> node)
        {
            Remove(node);
            Add(node);
        }
    }

    public class SlimLinkedListNode<T> : IDisposable
    {
        public T Value { get; set; }
        public SlimLinkedListNode<T> Previous { get; set; }
        public SlimLinkedListNode<T> Next { get; set; }

        public SlimLinkedListNode()
        {

        }

        public SlimLinkedListNode(T value)
        {
            Value = value;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    (Value as IDisposable)?.Dispose(); //Neat trick
                    Next = null;
                    Previous = null;
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SlimLinkedListNode() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
