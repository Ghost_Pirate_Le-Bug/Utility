﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityClasses;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            QueueingDictionary<string, int> test = new QueueingDictionary<string, int> { MaximumCacheSize = 3 };
            test.ToQueue();
            test.Add("aaa", 0);
            test.Add("bbb", 1);
            test.Add("ccc", 2);
            bool kaka = test.TryGetValue("bbb", out int res);
            test.Add("ddd", 3);
            test.Add("eee", 4);
            bool cAaa = test.ContainsKey("aaa");
            test.Remove("ccc");
            

        }
    }
}
